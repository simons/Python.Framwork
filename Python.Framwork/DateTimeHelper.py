#!/usr/bin/env python
# encoding: utf-8

"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: DateTimeHelper.py
@time: 2017/12/3 15:13
"""

from datetime import datetime

def getNow():
    return datetime.now()
