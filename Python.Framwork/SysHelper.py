#!/usr/bin/env python
# encoding: utf-8

"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: SysHelper.py
@time: 2017/12/2 11:37
"""
import sys


# region
def AddToPath(*path):
    if not sys.path.__contains__(path):
        sys.path.append(path)


# endregion

import SysHelper

print
help(SysHelper)
